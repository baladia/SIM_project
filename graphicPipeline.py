import numpy as np

class Fragment:
  def __init__(self, x : int, y : int, depth : float, coverage : float = 1.0):
    self.x = x
    self.y = y
    self.coverage = coverage
    self.depth = depth
    self.output = []

def edgeSide(p, v0, v1) : 
  return (p[0] - v0[0])*(v1[1] - v0[1]) - (p[1] - v0[1]) * (v1[0] - v0[0]) #The edge side function, derived from the linear algebra cross product

class GraphicPipeline:
  def __init__ (self, width, height):
    self.width = width
    self.height = height
    self.image = np.zeros((height, width, 3))
    self.depthBuffer = np.ones((height, width))

  def VertexShader(self, vertices, data) :
    outputVertices = np.zeros_like(vertices)
    for i in range(vertices.shape[0]) :
      x = vertices[i][0]
      y = vertices[i][1]
      z = vertices[i][2]
      w = 1.0

      vec = np.array([[x],[y],[z],[w]])

      vec = np.matmul(data['projMatrix'],np.matmul(data['viewMatrix'],vec))

      outputVertices[i][0] = vec[0]/vec[3]
      outputVertices[i][1] = vec[1]/vec[3]
      outputVertices[i][2] = vec[2]/vec[3]

    return outputVertices

  def Rasterizer(self, v0, v1, v2) :
    fragments = []
    samples_per_pixel = 4 #number of samples per pixel
    sample_offset = 1.0 / np.sqrt(samples_per_pixel) #calculating the offset of each sample (sub pixel)

    for j in range(self.height):
        for i in range(self.width):
          covered_samples = 0 #this will hold how many samples are covered (crucial for color rendering)
          #loop over each sub pixel
          for sy in range(0, int(np.sqrt(samples_per_pixel))):
            for sx in range(0, int(np.sqrt(samples_per_pixel))):
                x = (i + (sx + 0.5) * sample_offset) / self.width * 2 - 1
                y = (j + (sy + 0.5) * sample_offset) / self.height * 2 - 1

                p = np.array([x, y])

                if (edgeSide(p, v0, v1) >= 0 and edgeSide(p, v1, v2) >= 0 and edgeSide(p, v2, v0) >= 0):
                    covered_samples += 1

          if covered_samples > 0:
            #calculating the coverage
            coverage = covered_samples / samples_per_pixel
            fragment = Fragment(i, j, 0, coverage)
            fragments.append(fragment)
          
    return fragments
  
  def fragmentShader(self,fragment,data):
    #choose white color to visualize the final effect clearly
    triangle_color = np.array([1.0, 1.0, 1.0])
    #calculating the final color of each fragment's pixel
    fragment.output = fragment.coverage * triangle_color

  def draw(self, vertices, triangles, data):
    newVertices = self.VertexShader(vertices, data)

    fragments = []
    for t in triangles :
      #call the rasterizer the triangle t
      fragments.extend(self.Rasterizer(newVertices[t[0]], newVertices[t[1]], newVertices[t[2]]))

    for f in fragments:
      self.fragmentShader(f,data)
      #todo Process each fragment using the depth buffer
      if self.depthBuffer[f.y, f.x] > f.depth:
        self.depthBuffer[f.y, f.x] = f.depth
        self.image[f.y][f.x] = f.output